<?php

return function(string $name)
{
    $dirElements = explode('/', realpath(dirname(__FILE__)));
    array_pop($dirElements);
    define('ROOT_DIR', implode('/', $dirElements) . '/');
    define('BIN_DIR', ROOT_DIR . 'bin/');
    define('SRC_ROOT', ROOT_DIR . 'src/');
    define('BUILD_ROOT', ROOT_DIR . 'bin/');
    define('FILENAME', $name . '.phar');
    define('FILE', BUILD_ROOT . FILENAME);
    define('DEST_FILE', BUILD_ROOT . $name);

// The php.ini setting phar.readonly must be set to 0
    $pharFile = FILE;

// clean up
    if (file_exists($pharFile))
    {
        unlink($pharFile);
    }
    if (file_exists($pharFile . '.gz'))
    {
        unlink($pharFile . '.gz');
    }

// create phar
    $phar = new Phar($pharFile);
// creating our library using whole directory
    $phar->buildFromDirectory(SRC_ROOT);
// pointing main file which requires all classes
    $defaultStub = $phar::createDefaultStub("phar/{$name}.php", '/index.php');
    $phar->setStub("#!/usr/bin/env php\n" . $defaultStub);
    $phar->compressFiles(Phar::GZ);
    
    echo PHP_EOL . "{$pharFile} successfully created";
    rename(FILE, DEST_FILE);
    shell_exec('chmod +x ' . DEST_FILE);
    echo PHP_EOL . 'File renamed to ' . DEST_FILE . ' and make it executable.';
};