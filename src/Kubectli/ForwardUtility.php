<?php

namespace Rafko1990\Scripts\Kubectli;

use Rafko1990\Scripts\Kubectli\Argument\ArgumentInterface;
use Rafko1990\Scripts\Kubectli\Argument\CustomNameArgument;
use Rafko1990\Scripts\Kubectli\Argument\PodNameArgument;
use Rafko1990\Scripts\Kubectli\Argument\PortArgument;
use Rafko1990\Scripts\Output\Console\Console;
use Rafko1990\Scripts\Output\Console\ConsoleTextStylize;

class ForwardUtility
{
    /**
     * @var string
     */
    private $token;
    
    /**
     * @var Console
     */
    private $console;
    
    /**
     * @var ConsoleTextStylize
     */
    private $consoleTextStylize;
    
    /**
     * @var ArgumentInterface[]
     */
    private $arguments = [];
    
    private $logoPrinted = false;
    
    public function __construct(string $token, Console $console, array $arguments = [])
    {
        $this->token = $token;
        $this->console = $console;
        $this->consoleTextStylize = new ConsoleTextStylize($console);
        $this->initArguments($arguments);
    }
    
    private function initArguments(array $arguments): void
    {
        $custom = new CustomNameArgument($this->console, $this->consoleTextStylize);
        $pod = new PodNameArgument($this->console, $this->consoleTextStylize);
        $port = new PortArgument($this->console, $this->consoleTextStylize, $pod);
        
        $this->arguments = [
            $custom,
            $pod,
            $port
        ];
        
        $args = [];
        foreach ($this->arguments as $argument)
        {
            $args[] = $argument->getName();
        }

        if (count(array_diff($args, array_keys($arguments))))
        {
            $this->printLogo();
        }
        
        foreach ($arguments as $key => $parsedArg)
        {
            foreach ($this->arguments as $argument)
            {
                if ($key === $argument->getName())
                {
                    $argument->setValue($parsedArg);
                }
            }
        }
    }
    
    protected function run(): void
    {
        $custom = $this->getArgumentValue('custom');
        $pod = $this->getArgumentValue('pod');
        $port = $this->getArgumentValue('port');
        $file = sprintf('/tmp/%s_%s.kubeconfig.conf', $custom, time());
        $token = $this->token;
        $projectInfo = shell_exec("curl -s -H \"PRIVATE-TOKEN:{$token}\" -H \"Content-Type:application/json\" -X GET https://gitlab.i-systems.pl/api/v4/projects/custom%2F{$custom}");
        $project = json_decode($projectInfo, true);
        $cmd = sprintf(
            'curl -s -H "PRIVATE-TOKEN:%s" -H "Content-Type:application/json" -X GET https://gitlab.i-systems.pl/api/v4/projects/%d/repository/files/.kubeconfig.conf/raw?ref=master > %s',
            $token,
            $project['id'],
            $file
        );
        shell_exec($cmd);
        list($port1) = explode(':', $port);
        $this->console->newLine()->print($this->consoleTextStylize->withBold()->withColorLightYellow()->getStyledText('You can browse it: '));
        if ($pod === 'solr')
        {
            $this->console->newLine()->print($this->consoleTextStylize->withBold()->withColorLightCyan()->getStyledText("http://localhost:{$port1}/solr/"));
        }
        if ($pod === 'mysql')
        {
            $this->console->newLine()->print($this->consoleTextStylize->withBold()->withColorLightCyan()->getStyledText("mysql://isklep:isklep@127.0.0.1:{$port1}/isklep"));
            $this->console->newLine()->print($this->consoleTextStylize->withBold()->withColorLightYellow()->getStyledText('Copy it and paste into Google Chrome'));
        }
        
        $cmd = sprintf('kubectl --kubeconfig=%s port-forward deployment/%s %s', $file, $pod, $port);
        $this->console->newLine()->print($this->consoleTextStylize->withBold()->withColorLightRed()->getStyledText('Exec: ' . $cmd));
        
        shell_exec($cmd);
    }
    
    private function printLogo(): void
    {
        $logo = "
       ___    ___    _________   ___     ___
      /ku/|  /be/|  /forward /| /  /| u /  /|
      ███ | /███/   █████████/  ███ | t ███ |
      ███ |/███/    ███ |___    ███ | i ███ |
      ███/ ███\     ███/   /|   ███ | l ███ |
      ███████  \    ███████/    ███ |   ███ |
      ███ |███  \   ███ |       ███ \___███ |
      ███ | ███ /|  ███ |       ███/ v1 ███/
      ███/   ███/   ███/         ████████/
";
        $lines = explode(PHP_EOL, $logo);
        
        foreach($lines as $line){
            $this->console->print($this->consoleTextStylize->withColorYellow()->getStyledText($line))->newLine();
        }
        $this->logoPrinted = true;
    }
    
    private function printHelp(): void
    {
        $console = $this->console;
        $style = new ConsoleTextStylize($console);
        $console->newLine();
        $console->print('			' . $style->withColorLightBlue()->withBold()->getStyledText('Kube Forward Utility'));
        $console->newLine();
        $console->newLine();
        $console->print($style->withBold()->getStyledText('Usage: kfu'));
        foreach ($this->arguments as $argument)
        {
            $console->print($argument->getShortDescription());
        }
        $console->newLine();
        $console->print($style->getStyledText('------- Available options -------'));
        $console->newLine();
        foreach ($this->arguments as $argument)
        {
            $console->print($argument->getLongDescription());
            $console->newLine();
        }
        $console->newLine();
        $console->print('	' . $style->withColorRed()->withBackgroundColorLightYellow()->withBold()
                ->getStyledText(' The user will be asked to put the missing parameters. '));
        $console->newLine();
        $console->newLine();
    }
    
    private function getArgumentValue(string $name): ?string
    {
        foreach ($this->arguments as $argument)
        {
            if ($argument->getName() === $name)
            {
                return $argument->getValue();
            }
        }
        
        throw new \InvalidArgumentException('Invalid argument name.');
    }
    
    private function isCallingHelp(): bool
    {
        global $argv;
        $helpArray = [
            'help',
            '-help',
            '--help'
        ];
        
        if (empty(array_intersect($argv, $helpArray)))
        {
            return false;
        }
        
        return true;
    }
    
    public function execute(): void
    {
        if ($this->isCallingHelp())
        {
            $this->printHelp();
        }
        else
        {
            $this->run();
        }
    }
}
