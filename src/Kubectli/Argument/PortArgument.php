<?php

namespace Rafko1990\Scripts\Kubectli\Argument;

use Rafko1990\Scripts\Common\Argument\AbstractArgument;
use Rafko1990\Scripts\Common\Argument\ArgumentInterface;

class PortArgument extends AbstractArgument
{
    public function getName(): string
    {
        return 'port';
    }
    
    protected function getCustomTextStyled(string $text): string
    {
        return $this->consoleTextStylize->withBold()->withColorLightGreen()->getStyledText($text);
    }
    
    public function getShortDescription(): string
    {
        return $this->getCustomTextStyled(' [-port <local port>]');
    }
    
    public function getLongDescription(): string
    {
        return $this->getOptionDescriptionTextStyled('port', 'local port forwarding ex. 8983.');
    }
    
    public function getDefaultValue(): string
    {
        switch ($this->getRequireDependArgument()->getValue())
        {
            case 'solr':
                return 8983;
            case 'mysql':
                return 3306;
            default:
                throw new \InvalidArgumentException('Unsupported pod.');
        }
    }
    
    public function setValue(?string $value): void
    {
        if (false === strpos(':', $value) && false === empty ($value))
        {
            $value .= ':' . $this->getDefaultValue();
        }
        
        parent::setValue($value);
    }
    
    protected function askForValue(): string
    {
        do
        {
            $port = (int)$this->defaultAskForValue();
            
            if ($port < 1024 || $port > 65535)
            {
                $this->console->newLine()->print($this->getDefaultTextStyled(sprintf('Selecting port %d is invalid. Please select port between 1024 and 65535.', $port)));
            }
        }
        while ($port < 1024 || $port > 65535);
        
        
        $foundFreePort = false;
        
        while ($foundFreePort !== true)
        {
            if ($this->isPortFree($port))
            {
                $foundFreePort = true;
            }
            else
            {
                $port++;
                $this->console->newLine()->print($this->getDefaultTextStyled(sprintf('Selecting new port %d.', $port)));
            }
        }
        
        return sprintf('%d:%d', $port, $this->getPodPort());
    }
    
    function isPortFree(int $port): bool
    {
        $this->console->newLine();
        $this->console->print($this->getDefaultTextStyled(sprintf('Checking port %d.', $port)));
        
        $host = 'localhost';
        $connection = @fsockopen($host, $port, $errno, $errstr, 2);
        
        if (is_resource($connection))
        {
            fclose($connection);
            $this->console->print($this->getDefaultTextStyled(" Port {$port} is not available (already in use) ..."));
            
            return false;
        }
        else
        {
            $this->console->print($this->getDefaultTextStyled(" Port {$port} is available."));
            
            return true;
        }
    }
    
    protected function getQuestion(): string
    {
        $port = $this->getDefaultValue();
        
        return sprintf('Get forwarding ports [default: %d]', $port);
    }
    
    private function getPodPort()
    {
        return $this->getDefaultValue();
    }
    
    protected function validateDependArgument(ArgumentInterface $argument = null): void
    {
        if (null === $argument)
        {
            throw new \InvalidArgumentException('Require pod argument.');
        }
    }
}
