<?php

namespace Rafko1990\Scripts\Kubectli\Argument;

use Rafko1990\Scripts\Common\Argument\AbstractArgument;

class PodNameArgument extends AbstractArgument
{
    public function getName(): string
    {
        return 'pod';
    }
    
    protected function getCustomTextStyled(string $text): string
    {
        return $this->consoleTextStylize->withBold()->withColorLightMagenta()->getStyledText($text);
    }
    
    public function getShortDescription(): string
    {
        return $this->getCustomTextStyled(' [-pod <pod>]');
    }
    
    public function getLongDescription(): string
    {
        return $this->getOptionDescriptionTextStyled('pod', 'forwarding pod name: solr or mysql');
    }
    
    public function getDefaultValue(): string
    {
        return 'mysql';
    }
    
    protected function askForValue(): string
    {
        return $this->defaultAskForValue();
    }
    
    protected function getQuestion(): string
    {
        return 'Get forward pod name (solr or mysql) [default: mysql]';
    }
}
