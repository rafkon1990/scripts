<?php

namespace Rafko1990\Scripts\Kubectli\Argument;

use Rafko1990\Scripts\Common\Argument\AbstractArgument;
use Rafko1990\Scripts\Common\Argument\ArgumentInterface;
use Rafko1990\Scripts\Output\Console\Console;
use Rafko1990\Scripts\Output\Console\ConsoleTextStylize;

class CustomNameArgument extends AbstractArgument
{
    /**
     * @var string
     */
    private $token;
    
    public function __construct(string $token, Console $console, ConsoleTextStylize $consoleTextStylize, ArgumentInterface $argument = null)
    {
        $this->token = $token;
        parent::__construct($console, $consoleTextStylize, $argument);
    }
    
    public function getName(): string
    {
        return 'custom';
    }
    
    protected function getCustomTextStyled(string $text): string
    {
        return $this->consoleTextStylize->withBold()->withColorLightCyan()->getStyledText($text);
    }
    
    public function getShortDescription(): string
    {
        return $this->getCustomTextStyled(' [-custom <custom>]');
    }
    
    public function getLongDescription(): string
    {
        return $this->getOptionDescriptionTextStyled('custom', 'custom name, ex. template');
    }
    
    public function getDefaultValue(): string
    {
        return 'template';
    }
    
    protected function askForValue(): string
    {
        $custom = $this->defaultAskForValue();
        $this->console->newLine()->print('Validating custom ... ');
        $token = $this->token;
        $projectInfoJson = shell_exec("curl -s -H \"PRIVATE-TOKEN:{$token}\" -H \"Content-Type:application/json\" -X GET https://gitlab.i-systems.pl/api/v4/projects/custom%2F{$custom}");
        $projectInfo = json_decode($projectInfoJson, true);
        
        if (isset($projectInfo['message']) && $projectInfo['message'] === '404 Project Not Found')
        {
            $this->console->print('Invalid custom.');
            
            return $this->askForValue();
        }
        
        return $custom;
    }
    
    protected function getQuestion(): string
    {
        return 'Get custom name (ex. template, dalia, inoxa) [default: template]';
    }
}
