<?php

namespace Rafko1990\Scripts\Output;

interface PrintableInterface
{
    public function getWidth(): int;
    
    public function getHeight(): int;
    
    public function print(string $text): PrintableInterface;
    
    public function newLine(): PrintableInterface;
}
