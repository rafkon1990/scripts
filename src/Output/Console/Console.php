<?php

namespace Rafko1990\Scripts\Output\Console;

use Rafko1990\Scripts\Output\PrintableInterface;

class Console implements PrintableInterface
{
    /**
     * @var int
     */
    private $width;
    
    /**
     * @var int
     */
    private $height;
    
    public function __construct(int $width = 80, $height = 25)
    {
        $this->width = $width;
        $this->height = $height;
    }
    
    public function getWidth(): int
    {
        return $this->width;
    }
    
    public function getHeight(): int
    {
        return $this->height;
    }
    
    public function print(string $text): PrintableInterface
    {
        $lines = str_split($text, $this->getWidth());
        
        foreach ($lines as $key => $line)
        {
            if ($key > 0)
            {
                $this->newLine();
                $line = trim($line);
            }
            
            print $line;
        }
        
        return $this;
    }
    
    public function newLine(): PrintableInterface
    {
        print PHP_EOL;
    
        return $this;
    }
}
