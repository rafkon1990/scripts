<?php

namespace Rafko1990\Scripts\Output\Console;

class ArgumentReader
{
    private const INPUT_ARG_KEY_CHAR = '-';
    private const INPUT_ARG_VALUE_DELIMITER = '';
    
    public function parse(array $arguments): array
    {
        $parsedArguments = [];
        $parsedArguments['filename'] = array_shift($arguments);
        
        while ($argument = array_shift($arguments))
        {
            if ($this->isArgumentValue($argument))
            {
                $argumentValue = $this->getParsedValue($argument);
                
                if (isset($previousArgumentKey) && is_null($parsedArguments[$previousArgumentKey]))
                {
                    $parsedArguments[$previousArgumentKey] = $argumentValue;
                    unset($previousArgumentKey);
                }
                else
                {
                    $parsedArguments[] = $argumentValue;
                }
            }
            
            if ($this->isArgumentKey($argument))
            {
                $argumentKey = $this->getParsedKey($argument);
                $parsedArguments[$argumentKey] = null;
                $previousArgumentKey = $argumentKey;
            }
        }
        
        return $parsedArguments;
    }
    
    private function isArgumentKey(string $argument): bool
    {
        return false !== strpos($argument, self::INPUT_ARG_KEY_CHAR, 0);
    }
    
    private function isArgumentValue(string $argument): bool
    {
        return false === $this->isArgumentKey($argument);
    }
    
    private function getParsedKey(string $argument): string
    {
        return ltrim($argument, static::INPUT_ARG_KEY_CHAR);
    }
    
    private function getParsedValue(string $argument): string
    {
        return ltrim($argument, static::INPUT_ARG_VALUE_DELIMITER);
    }
}