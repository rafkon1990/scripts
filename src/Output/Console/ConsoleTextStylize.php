<?php

namespace Rafko1990\Scripts\Output\Console;

use Rafko1990\Scripts\Output\PrintableInterface;

/**
 * @method ConsoleTextStylize withCenter()
 *
 * @method ConsoleTextStylize withBold()
 * @method ConsoleTextStylize withDark()
 * @method ConsoleTextStylize withItalic()
 * @method ConsoleTextStylize withUnderline()
 * @method ConsoleTextStylize withBlink()
 * @method ConsoleTextStylize withReverse()
 * @method ConsoleTextStylize withConcealed()
 * @method ConsoleTextStylize withTab()
 *
 * @method ConsoleTextStylize withColorDefault()
 * @method ConsoleTextStylize withColorBlack()
 * @method ConsoleTextStylize withColorRed()
 * @method ConsoleTextStylize withColorGreen()
 * @method ConsoleTextStylize withColorYellow()
 * @method ConsoleTextStylize withColorBlue()
 * @method ConsoleTextStylize withColorMagenta()
 * @method ConsoleTextStylize withColorCyan()
 * @method ConsoleTextStylize withColorLightGray()
 * @method ConsoleTextStylize withColorDarkGray()
 * @method ConsoleTextStylize withColorLightRed()
 * @method ConsoleTextStylize withColorLightGreen()
 * @method ConsoleTextStylize withColorLightYellow()
 * @method ConsoleTextStylize withColorLightBlue()
 * @method ConsoleTextStylize withColorLightMagenta()
 * @method ConsoleTextStylize withColorLightCyan()
 * @method ConsoleTextStylize withColorWhite()
 *
 * @method ConsoleTextStylize withBackgroundColorDefault()
 * @method ConsoleTextStylize withBackgroundColorBlack()
 * @method ConsoleTextStylize withBackgroundColorRed()
 * @method ConsoleTextStylize withBackgroundColorGreen()
 * @method ConsoleTextStylize withBackgroundColorYellow()
 * @method ConsoleTextStylize withBackgroundColorBlue()
 * @method ConsoleTextStylize withBackgroundColorMagenta()
 * @method ConsoleTextStylize withBackgroundColorCyan()
 * @method ConsoleTextStylize withBackgroundColorLightGray()
 * @method ConsoleTextStylize withBackgroundColorDarkGray()
 * @method ConsoleTextStylize withBackgroundColorLightRed()
 * @method ConsoleTextStylize withBackgroundColorLightGreen()
 * @method ConsoleTextStylize withBackgroundColorLightYellow()
 * @method ConsoleTextStylize withBackgroundColorLightBlue()
 * @method ConsoleTextStylize withBackgroundColorLightMagenta()
 * @method ConsoleTextStylize withBackgroundColorLightCyan()
 * @method ConsoleTextStylize withBackgroundColorWhite()
 */
class ConsoleTextStylize
{
    private const FORMATTING = array(
        'with_center'                         => 'applyCenter',
        // ASCII @see https://en.wikipedia.org/wiki/ANSI_escape_code
        // italic and blink may not work depending of your terminal
        'reset'                               => '0',
        'with_bold'                           => '1',
        'bold'                                => '1',
        'with_dark'                           => '2',
        'with_italic'                         => '3',
        'with_underline'                      => '4',
        'underline'                           => '4',
        'with_blink'                          => '5',
        'blink'                               => '5',
        'with_reverse'                        => '7',
        'with_concealed'                      => '8',
        'with_tab'                              => '8',
        'with_color_default'                  => '39',
        'with_color_black'                    => '30',
        'with_color_red'                      => '31',
        'with_color_green'                    => '32',
        'with_color_yellow'                   => '33',
        'with_color_blue'                     => '34',
        'with_color_magenta'                  => '35',
        'with_color_cyan'                     => '36',
        'with_color_light_gray'               => '37',
        'with_color_dark_gray'                => '90',
        'with_color_light_red'                => '91',
        'with_color_light_green'              => '92',
        'with_color_light_yellow'             => '93',
        'with_color_light_blue'               => '94',
        'with_color_light_magenta'            => '95',
        'with_color_light_cyan'               => '96',
        'with_color_white'                    => '97',
        'with_background_color_default'       => '49',
        'with_background_color_black'         => '40',
        'with_background_color_red'           => '41',
        'with_background_color_green'         => '42',
        'with_background_color_yellow'        => '43',
        'with_background_color_blue'          => '44',
        'with_background_color_magenta'       => '45',
        'with_background_color_cyan'          => '46',
        'with_background_color_light_gray'    => '47',
        'with_background_color_dark_gray'     => '100',
        'with_background_color_light_red'     => '101',
        'with_background_color_light_green'   => '102',
        'with_background_color_light_yellow'  => '103',
        'with_background_color_light_blue'    => '104',
        'with_background_color_light_magenta' => '105',
        'with_background_color_light_cyan'    => '106',
        'with_background_color_white'         => '107',
    );
    private const FORMAT_PATTERN = '#<([a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*)>(.*?)</\\1?>#s';
    private const ESC_SEQ_PATTERN = "\033[%sm";
    
    /**
     * @var array
     */
    private $appliedStyles = [];
    
    /**
     * @var Console
     */
    private $printable;
    
    public function __construct(PrintableInterface $printable)
    {
        $this->printable = $printable;
    }
    
    public function getStyledText(string $text): string
    {
        $styledText = preg_replace_callback(self::FORMAT_PATTERN, array($this, 'getStyledWithStyleTags'), $text);
        
        while ($style = array_shift($this->appliedStyles))
        {
            $styledText = $this->applyStyle($styledText, $style);
        }
        
        return $styledText;
    }
    
    private function getStyledWithStyleTags(array $matches): string
    {
        $cloned = new self($this->printable);
        $cloned->{$matches[1]}();
        
        return $cloned->getStyledText($matches[2]);
    }
    
    private function applyCenter(string $text): string
    {
        $centered = '';
        
        foreach (explode(PHP_EOL, $text) as $line)
        {
            $line = trim($line);
            $centered .= str_pad($line, $this->printable->getWidth(), ' ', STR_PAD_BOTH) . PHP_EOL;
        }
        
        return trim($centered, PHP_EOL);
    }
    
    public function __call(string $method, array $arguments = []): ConsoleTextStylize
    {
        if (method_exists($this, $method))
        {
            $this->$method($arguments);
        }
        
        $style = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $method));
        
        return $this->handleStyle($style);
    }
    
    public function reset(): ConsoleTextStylize
    {
        $this->appliedStyles = [];
        
        return $this;
    }
    
    private function handleStyle(string $style): ConsoleTextStylize
    {
        if (false === array_key_exists($style, static::FORMATTING))
        {
            return $this;
        }
        
        $this->appliedStyles[] = $style;
        
        return $this;
    }
    
    private function applyStyle(string $text, string $style): string
    {
        if (method_exists($this, static::FORMATTING[$style]))
        {
            $callableMethod = (static::FORMATTING[$style]);
            
            return $this->{$callableMethod}($text);
        }
        
        return sprintf(static::ESC_SEQ_PATTERN, static::FORMATTING[$style])
            . $text
            . sprintf(static::ESC_SEQ_PATTERN, static::FORMATTING['reset']);
    }
}