<?php

namespace Rafko1990\Scripts\Common\Argument;

use Rafko1990\Scripts\Output\Console\Console;
use Rafko1990\Scripts\Output\Console\ConsoleTextStylize;

abstract class AbstractArgument implements ArgumentInterface
{
    /**
     * @var Console
     */
    protected $console;
    
    /**
     * @var ConsoleTextStylize
     */
    protected $consoleTextStylize;
    
    /**
     * @var ArgumentInterface|null
     */
    private $dependArgument;
    
    /**
     * @var string|null
     */
    private $value;
    
    public function __construct(Console $console, ConsoleTextStylize $consoleTextStylize, ArgumentInterface $argument = null)
    {
        $this->console = $console;
        $this->consoleTextStylize = $consoleTextStylize;
        $this->dependArgument = $argument;
        $this->validateDependArgument($argument);
    }
    
    abstract protected function getCustomTextStyled(string $text): string;
    
    protected function getDefaultTextStyled(string $text): string
    {
        return $this->consoleTextStylize->withItalic()->withColorLightGray()->getStyledText($text);
    }
    
    protected function getOptionDescriptionTextStyled($optionName, $optionDescription): string
    {
        $optionStyled = $this->getCustomTextStyled("-{$optionName}");
        $length = strlen($optionStyled);
        
        if ($length < 20)
        {
            $optionStyled .= '      ';
        }
        elseif ($length < 30)
        {
            $optionStyled .= '  ';
        }
        
        return $optionStyled . $this->getDefaultTextStyled($optionDescription);
    }
    
    protected function getRequireDependArgument(): ?ArgumentInterface
    {
        return $this->dependArgument;
    }
    
    protected function validateDependArgument(ArgumentInterface $argument = null): void
    {
    }
    
    abstract protected function getDefaultValue(): string;
    
    public function setValue(?string $value): void
    {
        $this->value = $value;
    }
    
    public function getValue(): ?string
    {
        if (is_null($this->value))
        {
            $this->value = $this->askForValue();
        }
        
        return $this->value;
    }
    
    abstract protected function askForValue(): string;
    
    protected function defaultAskForValue(): string
    {
        $this->console->newLine()->print($this->getCustomTextStyled($this->getQuestion() . ': '));
        $reply = $this->handleReply();
        
        if (empty($reply))
        {
            $reply = $this->getDefaultValue();
        }
        
        return $reply;
    }
    
    protected function handleReply(): ?string
    {
        $handle = fopen("php://stdin", "r");
        $reply = trim(fgets($handle));
        fclose($handle);
        
        return $reply;
    }
    
    abstract protected function getQuestion(): string;
}