<?php

namespace Rafko1990\Scripts\Common\Argument;

interface ArgumentInterface
{
    public function getName(): string;
    
    public function getShortDescription(): string;
    
    public function getLongDescription(): string;
    
    public function setValue(string $value): void;
    
    public function getValue(): ?string;
}