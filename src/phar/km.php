<?php

use Rafko1990\Scripts\IFirma\Kilometrowka;
use Rafko1990\Scripts\Output\Console\ArgumentReader;
use Rafko1990\Scripts\Output\Console\Console;

require_once '../Output/PrintableInterface.php';
require_once '../Output/Console/ArgumentReader.php';
require_once '../Output/Console/Console.php';
require_once '../Output/Console/ConsoleTextStylize.php';
require_once '../IFirma/Kilometrowka.php';
require_once '../Common/Argument/ArgumentInterface.php';
require_once '../Common/Argument/AbstractArgument.php';
require_once '../IFirma/Argument/FileArgument.php';
require_once '../IFirma/Argument/MonthArgument.php';
require_once '../IFirma/Argument/YearArgument.php';

$token = 'PASTE TOKEN HERE';
$console = new Console(120, 25);
$argReader = new ArgumentReader();
$fu = new Kilometrowka($token, $console, $argReader->parse($argv));
$fu->execute();
