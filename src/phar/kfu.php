<?php

use Rafko1990\Scripts\Kubectli\ForwardUtility;
use Rafko1990\Scripts\Output\Console\ArgumentReader;
use Rafko1990\Scripts\Output\Console\Console;

require_once '../Output/PrintableInterface.php';
require_once '../Output/Console/ArgumentReader.php';
require_once '../Output/Console/Console.php';
require_once '../Output/Console/ConsoleTextStylize.php';
require_once '../Kubectli/ForwardUtility.php';
require_once '../Common/Argument/ArgumentInterface.php';
require_once '../Common/Argument/AbstractArgument.php';
require_once '../Kubectli/Argument/CustomNameArgument.php';
require_once '../Kubectli/Argument/PodNameArgument.php';
require_once '../Kubectli/Argument/PortArgument.php';

$token = 'PASTE TOKEN HERE';
$console = new Console(120, 25);
$argReader = new ArgumentReader();
$fu = new ForwardUtility($token, $console, $argReader->parse($argv));
$fu->execute();
