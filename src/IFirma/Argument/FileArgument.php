<?php

namespace Rafko1990\Scripts\IFirma\Argument;

use Rafko1990\Scripts\Common\Argument\AbstractArgument;

class FileArgument extends AbstractArgument
{
    public function getName(): string
    {
        return 'file';
    }
    
    protected function getCustomTextStyled(string $text): string
    {
        return $this->consoleTextStylize->withBold()->withColorLightMagenta()->getStyledText($text);
    }
    
    public function getShortDescription(): string
    {
        return $this->getCustomTextStyled(' [-file <file>]');
    }
    
    public function getLongDescription(): string
    {
        return $this->getOptionDescriptionTextStyled('file', 'A filename: ~/kilometrowka.csv');
    }
    
    public function getDefaultValue(): string
    {
        return '~/kilometrowka.csv';
    }
    
    protected function askForValue(): string
    {
        return $this->defaultAskForValue();
    }
    
    protected function getQuestion(): string
    {
        return sprintf('Get filename [default: %s]', $this->getDefaultValue());
    }
}
