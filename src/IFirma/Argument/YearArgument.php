<?php

namespace Rafko1990\Scripts\IFirma\Argument;

use Rafko1990\Scripts\Common\Argument\AbstractArgument;

class YearArgument extends AbstractArgument
{
    public function getName(): string
    {
        return 'year';
    }
    
    protected function getCustomTextStyled(string $text): string
    {
        return $this->consoleTextStylize->withBold()->withColorLightMagenta()->getStyledText($text);
    }
    
    public function getShortDescription(): string
    {
        return $this->getCustomTextStyled(' [-year <year>]');
    }
    
    public function getLongDescription(): string
    {
        return $this->getOptionDescriptionTextStyled('year', 'A year: 2018, 2019 etc');
    }
    
    public function getDefaultValue(): string
    {
        return date('Y');
    }
    
    protected function askForValue(): string
    {
        return $this->defaultAskForValue();
    }
    
    protected function getQuestion(): string
    {
        return sprintf('Get year [default: %s]', $this->getDefaultValue());
    }
}
