<?php

namespace Rafko1990\Scripts\IFirma\Argument;

use Rafko1990\Scripts\Common\Argument\AbstractArgument;

class MonthArgument extends AbstractArgument
{
    public function getName(): string
    {
        return 'month';
    }
    
    protected function getCustomTextStyled(string $text): string
    {
        return $this->consoleTextStylize->withBold()->withColorLightMagenta()->getStyledText($text);
    }
    
    public function getShortDescription(): string
    {
        return $this->getCustomTextStyled(' [-month <month>]');
    }
    
    public function getLongDescription(): string
    {
        return $this->getOptionDescriptionTextStyled('year', 'A month: 01, 02 ... 12');
    }
    
    public function getDefaultValue(): string
    {
        return date('m');
    }
    
    protected function askForValue(): string
    {
        return $this->defaultAskForValue();
    }
    
    protected function getQuestion(): string
    {
        return sprintf('Get month [default: %s]', $this->getDefaultValue());
    }
}
