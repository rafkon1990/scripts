<?php

namespace Rafko1990\Scripts\IFirma;

use Rafko1990\Scripts\Common\Argument\ArgumentInterface;
use Rafko1990\Scripts\IFirma\Argument\FileArgument;
use Rafko1990\Scripts\IFirma\Argument\MonthArgument;
use Rafko1990\Scripts\IFirma\Argument\YearArgument;
use Rafko1990\Scripts\Output\Console\Console;
use Rafko1990\Scripts\Output\Console\ConsoleTextStylize;

class Kilometrowka
{
    private const DATE_FORMAT = '%d-%d-%d';
    private const FROM = 'Pszów'; // Miejsce wyjazdu
    private const FROM_DESC = 'Dojazd do klienta.'; // Cel wyjazdu
    private const TO = 'Gliwice'; // Miejsce docelowe wyjazdu
    private const TO_DESC = 'Powrót od klienta.'; // Analogicznie do celu wyjazdu
    private const LENGTH = '41.00'; // ilość km
    
    private $holidays = [
        2018 => [
            1  => [1 => true, 6 => true],
            4  => [1 => true, 2 => true],
            5  => [1 => true, 3 => true, 20 => true, 31 => true],
            7  => [15 => true],
            11 => [1 => true, 11 => true, 12 => true],
            12 => [25 => true, 26 => true],
        
        ],
        2019 => [
            1  => [1 => true, 6 => true],
            4  => [21 => true, 22 => true],
            5  => [1 => true, 3 => true],
            6  => [9 => true, 20 => true],
            8  => [15 => true],
            11 => [1 => true, 11 => true],
            12 => [25 => true, 26 => true],
        ],
    ];
    /**
     * @var Console
     */
    private $console;
    
    /**
     * @var ConsoleTextStylize
     */
    private $consoleTextStylize;
    
    /**
     * @var ArgumentInterface[]
     */
    private $arguments = [];
    
    public function __construct(string $token, Console $console, array $arguments = [])
    {
        $this->token = $token;
        $this->console = $console;
        $this->consoleTextStylize = new ConsoleTextStylize($console);
        $this->initArguments($arguments);
    }
    
    private function initArguments(array $arguments): void
    {
        $fileArgument = new FileArgument($this->console, $this->consoleTextStylize);
        $yearArgument = new YearArgument($this->console, $this->consoleTextStylize);
        $monthArgument = new MonthArgument($this->console, $this->consoleTextStylize);
        
        $this->arguments = [
            $fileArgument,
            $yearArgument,
            $monthArgument,
        ];
        
        $args = [];
        foreach ($this->arguments as $argument)
        {
            $args[] = $argument->getName();
        }
        
        foreach ($arguments as $key => $parsedArg)
        {
            foreach ($this->arguments as $argument)
            {
                if ($key === $argument->getName())
                {
                    $argument->setValue($parsedArg);
                }
            }
        }
    }
    
    private function getWorkingDays(int $year, int $month): array
    {
        // https://daveismyname.blog/show-working-days-of-a-month-excluding-weekends-with-php
        $workdays = array();
        $day_count = \cal_days_in_month(CAL_GREGORIAN, $month, $year); // Get the amount of days
        
        //loop through all days
        for ($i = 1; $i <= $day_count; $i++)
        {
            $date = $year . '/' . $month . '/' . $i; //format date
            $get_name = date('l', strtotime($date)); //get week day
            $day_name = substr($get_name, 0, 3); // Trim day name to 3 chars
            
            //if not a weekend add day to array
            if ($day_name != 'Sun' && $day_name != 'Sat')
            {
                $workdays[] = $i;
            }
        }
        
        return $workdays;
    }
    
    private function getCsvHeaders(): array
    {
        return [
            'Data',
            'Z',
            'Do',
            'Cel wyjazdu',
            'Ilość KM',
        ];
    }
    
    private function getRowFrom(int $year, int $month, int $day): array
    {
        return [
            sprintf(self::DATE_FORMAT, $day, $month, $year),
            self::FROM,
            self::TO,
            self::FROM_DESC,
            self::LENGTH
        ];
    }
    
    private function getRowTo(int $year, int $month, int $day): array
    {
        return [
            sprintf(self::DATE_FORMAT, $day, $month, $year),
            self::TO,
            self::FROM,
            self::TO_DESC,
            self::LENGTH
        ];
    }
    
    private function isHoliday(int $year, int $month, int $day): bool
    {
        if (false == isset($this->holidays[$year]))
        {
            return false;
        }
        
        if (false == isset($this->holidays[$year][$month]))
        {
            return false;
        }
        
        if (false == isset($this->holidays[$year][$month][$day]))
        {
            return false;
        }
        
        return $this->holidays[$year][$month][$day] === true;
    }
    
    private function generateCsvFile(string $file, int $year, int $month): void
    {
        $rows = [];
        $rows[] = $this->getCsvHeaders();
        $workingDays = $this->getWorkingDays($year, $month);
        
        foreach ($workingDays as $workingDay)
        {
            if ($this->isHoliday($year, $month, $workingDay))
            {
                $this->console
                    ->newLine()
                    ->print('Skipping holiday: ' . sprintf(self::DATE_FORMAT, $workingDay, $month, $year));
                continue;
            }
            
            $rows[] = $this->getRowFrom($year, $month, $workingDay);
            $rows[] = $this->getRowTo($year, $month, $workingDay);
        }
        $fileHandle = fopen($file, 'w');
        
        if (false === $fileHandle)
        {
            $this->console
                ->newLine()
                ->print('Could not get file.');
            
            return;
        }
        
        foreach ($rows as $row)
        {
            fputcsv($fileHandle, $row);
        }
        fclose($fileHandle);
    }
    
    protected function run(): void
    {
        $fileArgument = $this->getArgumentValue('file');
        $yearArgument = $this->getArgumentValue('year');
        $monthArgument = $this->getArgumentValue('month');
        $this->generateCsvFile($fileArgument, $yearArgument, $monthArgument);
    }
    
    private function printHelp(): void
    {
        $console = $this->console;
        $style = new ConsoleTextStylize($console);
        $console->newLine();
        $console->print('			' . $style->withColorLightBlue()->withBold()->getStyledText('Kilometrówka'));
        $console->newLine();
        $console->newLine();
        $console->print($style->withBold()->getStyledText('Usage: Kilometrowka'));
        foreach ($this->arguments as $argument)
        {
            $console->print($argument->getShortDescription());
        }
        $console->newLine();
        $console->print($style->getStyledText('------- Available options -------'));
        $console->newLine();
        foreach ($this->arguments as $argument)
        {
            $console->print($argument->getLongDescription());
            $console->newLine();
        }
        $console->newLine();
        $console->print('	' . $style->withColorRed()->withBackgroundColorLightYellow()->withBold()
                ->getStyledText(' The user will be asked to put the missing parameters. '));
        $console->newLine();
        $console->newLine();
    }
    
    private function getArgumentValue(string $name): ?string
    {
        foreach ($this->arguments as $argument)
        {
            if ($argument->getName() === $name)
            {
                return $argument->getValue();
            }
        }
        
        throw new \InvalidArgumentException('Invalid argument name.');
    }
    
    private function isCallingHelp(): bool
    {
        global $argv;
        $helpArray = [
            'help',
            '-help',
            '--help'
        ];
        
        if (empty(array_intersect($argv, $helpArray)))
        {
            return false;
        }
        
        return true;
    }
    
    public function execute(): void
    {
        if ($this->isCallingHelp())
        {
            $this->printHelp();
        }
        else
        {
            $this->run();
        }
    }
}
